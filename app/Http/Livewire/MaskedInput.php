<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Manny;

class MaskedInput extends Component
{
    public $cpf;
    public $cnpj;

    public function updated($field)
    {
        if ($field == 'cpf')
		{
			$this->cpf = Manny::stripper($this->phone, "111.111.111.-11");
		}
        if ($field == 'cnpj')
		{
			$this->cnpj = Manny::stripper($this->phone, "11.111.111/1111.-11");
		}
    }
    public function render()
    {
        return view('livewire.masked-input');
    }
}
