<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'tipo',
        'nome',
        'nascimento',
        'cpf_cnpj',
        'rg',
        'rg_org_exp',
        'estado_civil',
        'nacionalidade',
        'profissao',
        'endereco',
        'bairro',
        'cidade',
        'uf',
        'cep',
        'telefone1',
        'telefone2',
        'telefone3',
        'mail',
    ];
}
