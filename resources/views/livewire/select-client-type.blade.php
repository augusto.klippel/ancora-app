<div>
    <div class="grid grid-cols-3">
        {{-- <x-label for="tipo" value="Tipo" /> --}}
        <x-form-group label="Tipo" name="tipo">
            <x-select name="tipo" id="tipo" wire:model="tipoPessoa" :options="['Física' => 'Física', 'Jurídica' => 'Jurídica']" />
        </x-form-group >
        {{-- {{ $tipoPessoa }} --}}
    </div>
    @if($tipoPessoa == 'Física')
    <div class="grid grid-cols-3">
        <x-form-group label="CPF" name="cpf">
            {{-- <x-input name="cpf" id="cpf" type="text" /> --}}
            <x-input type="text" name="cpf" id="cpf" wire:model="cpf" />
        </x-form-group>
        <x-form-group label="Telefone 1" name="telefone1" class="ml-3">
            <x-input type="text" name="telefone1" id="telefone1" wire:model="telefone1" />
        </x-form-group>
    </div>
    @else
    <div class="grid grid-cols-3">
        <x-form-group label="CNPJ" name="cnpj">
            {{-- <x-input name="cnpj" id="cnpj" type="text" /> --}}
            <x-input type="text" name="cnpj" id="cnpj" wire:model="cnpj" />

        </x-form-group>
    </div>
    @endif
</div>

