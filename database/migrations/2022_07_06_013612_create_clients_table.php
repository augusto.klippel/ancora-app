<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->enum('tipo', ['Física', 'Jurídica']);
            $table->string('nome');
            $table->string('cpf_cnpj', 20)->unique();
            $table->string('rg')->nullable();
            $table->string('rg_org_exp')->nullable();
            $table->enum('estado_civil', ['Solteiro(a)', 'Casado(a)','Separado(a)', 'Divorciado(a)', 'Viúvo(a)', 'Outros']);
            $table->string('nacionalidade')->nullable();
            $table->string('profissao')->nullable();
            $table->string('endereco')->nullable();
            $table->string('bairro')->nullable();
            $table->string('cidade')->nullable();
            $table->string('uf', 2)->nullable();
            $table->string('cep', 10)->nullable();
            $table->string('telefone1', 20)->nullable();
            $table->string('telefone2', 20)->nullable();
            $table->string('telefone3', 20)->nullable();
            $table->string('mail')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
};
