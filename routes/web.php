<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

Route::prefix('client')->controller(ClientController::class)->middleware(['auth'])->group(function () {
    Route::get('/', 'index')->name('client_index');
    Route::get('/add', 'create')->name('client_create');
    Route::post('/add', 'store')->name('client_store');
    // Route::get('/orders/{id}', 'show');
    // Route::post('/orders', 'store');
});
Route::prefix('user')->controller(UserController::class)->middleware(['auth'])->group(function () {
    Route::get('/profile', 'profile')->name('user_profile');
    Route::get('/', 'index')->name('user_index');
    Route::post('/update', 'update')->name('user_update');
    Route::get('/add', 'create')->name('user_create');
    Route::post('/add', 'store')->name('user_store');
    Route::get('/del/{id}', 'show')->name('user_delete');
    Route::post('/del', 'destroy')->name('user_destroy');
});

require __DIR__.'/auth.php';
