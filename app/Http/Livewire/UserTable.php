<?php

namespace App\Http\Livewire;

use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Columns\LinkColumn;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\Views\Columns\ImageColumn;
use Rappasoft\LaravelLivewireTables\Views\Columns\ButtonGroupColumn;
use Rappasoft\LaravelLivewireTables\Views\Filters\MultiSelectFilter;
use Rappasoft\LaravelLivewireTables\Views\Filters\DateFilter;
use App\Models\User;

class UserTable extends DataTableComponent
{
    protected $model = User::class;

    // public function configure(): void
    // {
    //     $this->setPrimaryKey('id');
    // }

    public function configure(): void
    {

        $this->setPrimaryKey('id');

    }

    public function columns(): array
    {
        return [
            Column::make("#", "id")
                ->sortable(),
            Column::make("Nome", "name")
                ->sortable()
                ->searchable(),
            Column::make("E-mail", "email")
                ->sortable()
                ->searchable(),
            Column::make('Avatar', "image")
                ->format(
                    fn($value, $row, Column $column) => '<img src="'.asset('images/'.$row->image).'" class="w-8 h-8 rounded-full" />'
                )
                ->html(),
            Column::make("Criação", "created_at")
                ->sortable(),
            // Column::make("Updated at", "updated_at")
            //     ->sortable(),
            ButtonGroupColumn::make('Ações')
                ->buttons([
                    LinkColumn::make('Excluir')
                        ->title(fn($row) => 'Excluir')
                        ->location(fn($row) => route('user_delete', $row))
                        ->attributes(fn($row) => [
                            'class' => 'inline-flex items-center px-4 py-2 bg-red-500 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-red-700 active:bg-red-900 focus:outline-none focus:bg-red-900 focus:ring ring-green-300 disabled:opacity-25 transition ease-in-out duration-150',
                        ]),
                    ]),
                ];

    }
}
