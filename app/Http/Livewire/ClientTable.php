<?php

namespace App\Http\Livewire;

use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use App\Models\Client;

class ClientTable extends DataTableComponent
{
    protected $model = Client::class;

    public function configure(): void
    {
        $this->setPrimaryKey('id');
    }

    public function columns(): array
    {
        return [
            Column::make("Tipo", "tipo")
                ->sortable(),
            Column::make("Nome", "nome")
                ->sortable()
                ->searchable(),
            Column::make("Nascimento", "nascimento")
                ->sortable(),
            Column::make("Cpf/cnpj", "cpf_cnpj")
                ->sortable()
                ->searchable(),
            Column::make("RG", "rg")
                ->sortable()
                ->searchable(),
            Column::make("Telefone 1", "telefone1")
                ->sortable(),
            Column::make("Telefone 2", "telefone2")
                ->sortable(),
            Column::make("Telefone 3", "telefone3")
                ->sortable(),
            Column::make("E-mail", "mail")
                ->sortable()
                ->searchable(),
        ];
    }
}
