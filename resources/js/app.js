import './bootstrap';

import Alpine from 'alpinejs';

window.Alpine = Alpine;

Alpine.start();

window.onload = function() {
    setTimeout(function(){
        document.getElementById('status').style.display = 'none';
    }, 5000);
};
