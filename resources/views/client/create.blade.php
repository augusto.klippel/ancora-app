<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Novo Cliente
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />

        <form method="POST" action="{{ route('client_store') }}" enctype="multipart/form-data">
            @csrf
            <!-- Email Address -->
            <div>
                <x-label for="name" value="Nome" />

                <x-input id="name" class="block mt-1 w-full" type="text" name="name" value="" required autofocus />
            </div>
            <div class="mt-3">
                <livewire:select-client-type />
            </div>
            <div class="mt-3">
                <x-label for="name" value="E-mail" />

                <x-input id="email" class="block mt-1 w-full" type="email" name="email" value="" required />
            </div>
            <div class="mt-4">
                <x-label for="password" value="Senha" />

                <x-input id="password" class="block mt-1 w-full" type="text" name="password" value="mudar1234" />
            </div>

            <!-- Validation Errors -->
            <x-auth-validation-errors class="mb-4" :errors="$errors" />

            <div class="mt-8">
                <x-button-1 class="ml-4">
                    {{ __('Save') }}
                </x-button>
                <x-button-2 class="ml-3" onclick="history.go(-1)">
                    {{ __('Cancel') }}
                </x-button>
            </div>
        </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
