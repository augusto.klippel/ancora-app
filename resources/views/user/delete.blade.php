<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Exclusão de Usuário
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />

        <form method="POST" action="{{ route('user_destroy') }}">
            @csrf
            <div class="mt-4">
                <img src={{ asset('images/'.$user->image) }} title="{{ $user->name }}" class="block h-20 w-auto rounded-full fill-current text-gray-600" />
            </div>
            <!-- Email Address -->
            <div>
                <x-input id="id" type="hidden" name="id" value="{{ $user->id }}" />
                <x-label for="name" value="Nome" />

                <x-input id="name" class="block mt-1 w-full" type="text" name="name" value="{{ $user->name }}" disabled />
            </div>
            <div>
                <x-label for="name" value="E-mail" />

                <x-input id="email" class="block mt-1 w-full" type="email" name="email" value="{{ $user->email }}" disabled />
            </div>
            <div class="mt-8">
                <x-button-1 class="ml-4">
                    {{ __('Delete') }}
                </x-button>
                <x-button-2 class="ml-3" onclick="history.go(-1)">
                    {{ __('Cancel') }}
                </x-button>
            </div>
        </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
