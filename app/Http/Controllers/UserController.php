<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.index');
    }
    public function profile()
    {
        return view('user.profile');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:4',
            'email' => 'required|email',
            'password' => 'min:6',
        ]);
        $validated = $validator->validated();

        $user = new User;

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        if($request->file('image')){
            $file= $request->file('image');
            $filename= date('YmdHi')."_".$file->getClientOriginalName();
            $file-> move(public_path('images') , $filename);
            $user->image = $filename;
        }
        $user->save();

        return redirect()->route('user_index')->with('status', 'O usuário foi criado com Sucesso !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('user.delete', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if($request->password) {
            $validator = Validator::make($request->all(), [
                'password' => 'confirmed|min:6',
            ]);
            $validated = $validator->validated();
        }

        // dd($request);
        $user = User::find($request->id);
        $user->name = $request->name;
        if($request->file('image')){
            $file= $request->file('image');
            $filename= date('YmdHi')."_".$file->getClientOriginalName();
            $file-> move(public_path('images') , $filename);
            $user->image = $filename;
        }
        if($request->password) {
            $user->password = Hash::make($request->password);
        }
        $user->save();

        return redirect()->route('dashboard')->with('status', 'O usuário foi alterado com Sucesso !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        User::destroy($request->id);
        return redirect()->route('user_index')->with('status', 'O usuário foi excluído com Sucesso !');
    }
}
