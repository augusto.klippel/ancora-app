<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Log;
use Manny;

use Livewire\Component;

class SelectClientType extends Component
{
    public $tipoPessoa;
    public $cpf;
    public $cnpj;

    public function mount()
    {
        $this->tipoPessoa = 'Física';
    }

    public function render()
    {
        return view('livewire.select-client-type', [

            'tipoPessoa' => $this->tipoPessoa,

        ]);
    }
    public function updated($field)
    {
        if ($field == 'cpf')
		{
			$this->cpf = Manny::mask($this->cpf, "111.111.111-11");
		}
        if ($field == 'cnpj')
		{
			$this->cnpj = Manny::mask($this->cnpj, "11.111.111/1111-11");
		}
    }

}
